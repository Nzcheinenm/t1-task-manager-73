package ru.t1.dkononov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.dkononov.tm.api.service.UserService;
import ru.t1.dkononov.tm.entity.dto.UserDto;
import ru.t1.dkononov.tm.entity.model.User;
import ru.t1.dkononov.tm.mapper.UserMapper;
import ru.t1.dkononov.tm.repository.UserRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;


    @Override
    public List<UserDto> findAll() {
        return userRepository.findAll()
                .stream()
                .map(UserMapper::userToDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto save(final UserDto user) {
        return UserMapper.userToDto(userRepository.save(UserMapper.dtoToUser(user)));
    }

    @Override
    public UserDto findById(final String id) {
        final Optional<User> userDtoOptional = userRepository.findById(id);
        return userDtoOptional.map(UserMapper::userToDto).orElse(null);
    }

    @Override
    public boolean exsistsById(final String id) {
        return userRepository.existsById(id);
    }

    @Override
    public UserDto save() {
        final UserDto user = new UserDto();
        return UserMapper.userToDto(userRepository.save(UserMapper.dtoToUser(user)));
    }

    @Override
    public long count() {
        return userRepository.count();
    }

    @Override
    public void deleteById(final String id) {
        userRepository.deleteById(id);
    }

    @Override
    public void delete(final UserDto project) {
        userRepository.delete(UserMapper.dtoToUser(project));
    }

    @Override
    public void deleteAll(final List<UserDto> projectsDto) {
        List<User> users = new ArrayList<>();
        users = projectsDto
                .stream()
                .map(UserMapper::dtoToUser)
                .collect(Collectors.toList());
        userRepository.deleteAll(users);
    }

    @Override
    public void clear() {
        userRepository.deleteAll();
    }

}
