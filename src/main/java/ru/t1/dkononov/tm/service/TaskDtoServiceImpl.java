package ru.t1.dkononov.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.dkononov.tm.api.service.ProjectDtoService;
import ru.t1.dkononov.tm.api.service.TaskDtoService;
import ru.t1.dkononov.tm.api.service.UserService;
import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.entity.model.Project;
import ru.t1.dkononov.tm.entity.model.Task;
import ru.t1.dkononov.tm.entity.model.User;
import ru.t1.dkononov.tm.mapper.ProjectMapper;
import ru.t1.dkononov.tm.mapper.TaskMapper;
import ru.t1.dkononov.tm.mapper.UserMapper;
import ru.t1.dkononov.tm.repository.TaskRepository;
import ru.t1.dkononov.tm.utils.UserUtil;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskDtoServiceImpl implements TaskDtoService {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ProjectDtoService projectDtoService;

    @Override
    public List<TaskDto> findAll(final String userId) {
        return taskRepository.findAllByUserId(userId)
                .stream()
                .map(TaskMapper::taskToDto)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public TaskDto save(TaskDto taskDto) {
        final Task task = TaskMapper.dtoToTask(taskDto);
        final User user = UserMapper.dtoToUser(userService.findById(taskDto.getUserId()));
        if (taskDto.getProjectId() != null) {
            final Project project = ProjectMapper.dtoToProject(projectDtoService.findByUserIdAndId(taskDto.getUserId(), taskDto.getProjectId()));
            task.setProject(project);
        }
        task.setUser(user);
        return TaskMapper.taskToDto(taskRepository.save(task));
    }

    @Override
    public TaskDto save(final String userId) {
        final TaskDto taskDto = new TaskDto("New name " + LocalDateTime.now().toString());
        taskDto.setUserId(userId);
        final Task task = TaskMapper.dtoToTask(taskDto);
        final User user = UserMapper.dtoToUser(userService.findById(taskDto.getUserId()));
        task.setUser(user);
        return TaskMapper.taskToDto(taskRepository.save(task));
    }

    @Override
    public TaskDto findById(String id) {
        final Optional<Task> projectDtoOptional = taskRepository.findById(id);
        return projectDtoOptional.map(TaskMapper::taskToDto).orElse(null);
    }

    @Override
    public boolean exsistsById(String id) {
        return taskRepository.existsById(id);
    }

    @Override
    public TaskDto findByUserIdAndId(final String userId, final String id) {
        final Optional<Task> taskDtoOptional = taskRepository.findByUserIdAndId(userId, id);
        return taskDtoOptional.map(TaskMapper::taskToDto).orElse(null);
    }

    @Override
    public boolean exsistsByUserIdAndId(final String userId, final String id) {
        return taskRepository.findByUserIdAndId(UserUtil.getUserId(), id).isPresent();
    }

    @Override
    public void deleteByUserIdAndId(final String userId, final String id) {
        taskRepository.deleteByUserIdAndId(userId, id);
    }

    @Override
    public long count() {
        return taskRepository.count();
    }

    @Override
    @Transactional
    public void deleteById(String id) {
        taskRepository.deleteById(id);
    }

    @Override
    @Transactional
    public void delete(TaskDto task) {
        taskRepository.delete(TaskMapper.dtoToTask(task));
    }

    @Override
    @Transactional
    public void deleteAll(List<TaskDto> tasks) {
        List<Task> taskList = new ArrayList<>();
        taskList = tasks
                .stream()
                .map(TaskMapper::dtoToTask)
                .collect(Collectors.toList());
        taskRepository.deleteAll(taskList);
    }

    @Override
    @Transactional
    public void clear() {
        taskRepository.deleteAll();
    }


}
