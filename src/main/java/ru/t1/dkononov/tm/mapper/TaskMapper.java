package ru.t1.dkononov.tm.mapper;

import ru.t1.dkononov.tm.entity.dto.TaskDto;
import ru.t1.dkononov.tm.entity.model.Task;

public class TaskMapper {

    public static TaskDto taskToDto(final Task task) {
        TaskDto taskDto = new TaskDto();
        taskDto.setDateFinish(task.getDateFinish());
        taskDto.setDateStart(task.getDateStart());
        taskDto.setDescription(task.getDescription());
        taskDto.setName(task.getName());
        taskDto.setId(task.getId());
        if (task.getUser() != null) taskDto.setUserId(task.getUser().getId());
        if (task.getProject() != null) taskDto.setProjectId(task.getProject().getId());
        return taskDto;
    }

    public static Task dtoToTask(final TaskDto taskDto) {
        Task task = new Task();
        task.setDateFinish(taskDto.getDateFinish());
        task.setDateStart(taskDto.getDateStart());
        task.setDescription(taskDto.getDescription());
        task.setName(taskDto.getName());
        task.setId(taskDto.getId());
        return task;
    }

}
