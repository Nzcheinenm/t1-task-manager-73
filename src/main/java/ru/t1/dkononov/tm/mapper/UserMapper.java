package ru.t1.dkononov.tm.mapper;

import ru.t1.dkononov.tm.entity.dto.UserDto;
import ru.t1.dkononov.tm.entity.model.User;

public class UserMapper {

    public static UserDto userToDto(final User user) {
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setLogin(user.getLogin());
        userDto.setPasswordHash(user.getPasswordHash());
        userDto.setRoles(user.getRoles());
        return userDto;
    }

    public static User dtoToUser(final UserDto userDto) {
        User user = new User();
        user.setId(userDto.getId());
        user.setLogin(userDto.getLogin());
        user.setPasswordHash(userDto.getPasswordHash());
        user.setRoles(userDto.getRoles());
        return user;
    }

}
