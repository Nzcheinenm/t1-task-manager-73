package ru.t1.dkononov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.t1.dkononov.tm.entity.dto.TaskDto;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface TaskEndpoint {
    @WebMethod
    @GetMapping("/findAll")
    List<TaskDto> findAll();

    @WebMethod
    @PostMapping("/save")
    TaskDto save(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @GetMapping("/findById/{id}")
    TaskDto findById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/exsitsById/{id}")
    boolean exsistsById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @GetMapping("/count")
    long count();

    @WebMethod
    @PostMapping("/deleteById/{id}")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @PathVariable("id") String id
    );

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "task", partName = "task")
            @RequestBody TaskDto task
    );

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll(
            @WebParam(name = "tasks", partName = "tasks")
            @RequestBody List<TaskDto> tasks
    );

    @WebMethod
    @PostMapping("/clear")
    void clear();
}
